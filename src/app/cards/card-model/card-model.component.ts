import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CardService } from '../../services/card.service';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Card } from '../../models/card';

@Component({
  selector: 'app-card-model',
  templateUrl: './card-model.component.html',
  styleUrls: ['./card-model.component.scss'],
})
export class CardModelComponent implements OnInit {
  form!: FormGroup;
  loading !: boolean;

  constructor(
    private fb: FormBuilder,
    private cardService: CardService,
    private dialogRef: MatDialog,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: Card
  ) {}

  ngOnInit(): void {
    this.loading = true;

    this.form = this.fb.group({
      name: [this.data?.name || '', Validators.maxLength(50)],
      title: [
        this.data?.title || '',
        [Validators.required, Validators.maxLength(255)],
      ],
      phone: [
        this.data?.phone || '',
        [Validators.required, Validators.maxLength(20)],
      ],
      email: [
        this.data?.email || '',
        [Validators.email, Validators.maxLength(55)],
      ],
      address: [this.data?.address || '', Validators.maxLength(255)],
    });
  }

  addCard(): void {
    this.loading=true;
    this.cardService.addCard(this.form.value).subscribe((res: any) => {
      if (res) {
        this._snackBar.open('Kartvizit başarıyla eklendi', '', {
          duration: 4000,
        });

        this.cardService.getCards();
        this.dialogRef.closeAll();
        this.loading=false;
      }
    });
  }

  updateCard() : void {
    this.loading=true;
    this.cardService.updateCard(this.form.value,this.data.id).subscribe((res:any) => {
      if (res) {
        this._snackBar.open('Kartvizit başarıyla güncellendi', '', {
          duration: 4000,
        });

        this.cardService.getCards();
        this.dialogRef.closeAll();
        this.loading=false;
      }
    })
  }

  removeCard() : void {
    this.loading=true;
    this.cardService.removeCard(this.data.id).subscribe((res:any) => {
      if (res) {
        this._snackBar.open('Kartvizit başarıyla silindi.', '', {
          duration: 4000,
        });

        this.cardService.getCards();
        this.dialogRef.closeAll();
        this.loading=false;
      }
    })
  }
}
